/*
 * testingCalculateRPN.cpp
 *
 *  Created on: 04.06.2017
 *      Author: marce
 */

#include "gtest/gtest.h"
#include "calculateRPN.h"

TEST (TestingCalculateRPN, BasicOperations) {
	EXPECT_EQ(3, calculateRPN("1 2 +"));
	EXPECT_EQ(1, calculateRPN("2 1 -"));
	EXPECT_EQ(4, calculateRPN("2 2 *"));
	EXPECT_EQ(1, calculateRPN("2 2 /"));
}
TEST (TestingCalculateRPN, LongerValidExpressions) {
	EXPECT_EQ(23.5, calculateRPN("2 7 + 3 / 14 3 - 4 * + 2 /"));
	EXPECT_EQ(40, calculateRPN("12 2 3 4 * 10 5 / + * +"));
}
TEST (TestingCalculateRPN, ZeroOperandsExceptDividingByZero) {
	EXPECT_EQ(0, calculateRPN("0 0 +"));
	EXPECT_EQ(0, calculateRPN("0 0 -"));
	EXPECT_EQ(0, calculateRPN("0 0 *"));
	EXPECT_EQ(0, calculateRPN("0 1 /"));
}
TEST (TestingCalculateRPN, DivideByZero) {
	ASSERT_NO_FATAL_FAILURE(calculateRPN("0 0 /"));
	ASSERT_NO_FATAL_FAILURE(calculateRPN("1 0 /"));
	EXPECT_EQ(0, calculateRPN("0 0 /"));
	EXPECT_EQ(0, calculateRPN("1 0 /"));
}
TEST (TestingCalculateRPN, HandlesInvalidInput) {
	EXPECT_EQ(0, calculateRPN("1 +"));
	EXPECT_EQ(0, calculateRPN("1 -"));
	EXPECT_EQ(0, calculateRPN("* 1"));
	EXPECT_EQ(0, calculateRPN("/ 1"));
	EXPECT_EQ(0, calculateRPN("1 * 1"));
	EXPECT_EQ(0, calculateRPN("0 / 2"));
	EXPECT_EQ(0, calculateRPN("02+"));
	EXPECT_EQ(0, calculateRPN("2 3 + d"));
	EXPECT_EQ(0, calculateRPN("d / 2"));
}
