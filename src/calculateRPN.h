/*
 * calculateRPN.h
 *
 *  Created on: 04.06.2017
 *      Author: marce
 */

#ifndef CALCULATERPN_H_
#define CALCULATERPN_H_

#include <string>

// function calculates result of entered sequence written in Reverse Polish Notation
// if entered sequence is invalid function returns 0
double calculateRPN(std::string input);

#endif /* CALCULATERPN_H_ */
