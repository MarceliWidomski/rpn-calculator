/*
 * calculateRPN.cpp
 *
 *  Created on: 04.06.2017
 *      Author: marce
 */

#include "calculateRPN.h"
#include <cstdlib>
#include <stack>
#include <iostream>


double calculateRPN(std::string input) {
	input += " ";
	std::stack<double> numbers;
	std::string temporary;
	for (std::string::iterator it = input.begin(); it != input.end(); ++it) {
		if (*it >= '0' && *it <= '9') {
			temporary += *it;
		} else if (*it == ' ') {
			double value = std::atof(temporary.c_str());
			numbers.push(value);
			temporary.clear();
		} else if (numbers.size() > 1 && (*it == '*' || *it == '/' || *it == '+' || *it == '-')) {
			double result;
			double temp2 = numbers.top();
			numbers.pop();
			double temp1 = numbers.top();
			numbers.pop();
			switch (*it) {
			case '*':
				result = temp1 * temp2;
				break;
			case '/':
				if (!temp2) {
					std::cout << "Dividing by 0!\n";
					return 0;
				}
				result = temp1 / temp2;
				break;
			case '+':
				result = temp1 + temp2;
				break;
			case '-':
				result = temp1 - temp2;
				break;
			}
			numbers.push(result);
			++it;
		} else {
			std::cout << "Error: Entered sequence is invalid\n";
			return 0;
		}
	}
	return numbers.top();
}



