//============================================================================
// Name        : RPN.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>

#include "calculateRPN.h"
#include "gtest/gtest.h"

// gets input from console
std::string getInput();

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();

//	std::string input = getInput();
//	double result = calculateRPN(input);
//	std::cout << "result: " << result;
//	return 0;
}

std::string getInput() {
	std::string input;
	std::cout << "Enter math expression in reverse Polish notation:\n";
	std::getline(std::cin, input);
	return input;
}
